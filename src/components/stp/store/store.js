import Vue from 'vue'
import Vuex from 'vuex'
import Element from 'element-ui'
import {Notification} from 'element-ui'
import {i18n} from "../../../plugins/i18n";
import { mapState } from 'vuex'

Vue.use(Vuex);
Vue.use(Element);
Vue.prototype.$notify = Notification;

export const store = new Vuex.Store({
  state: {
    translations: {
      en: {
        "hello": "Hello World"
      },
      ar: {
        "hello": "Hallo Verden"
      }
    },
    loading: false,
    isLoading: false,
    fullPage: false,
    kpi_mtp: '',
    sectors:[],
    setTenant: [],
    orgapproveuser: [],
    communicationuser: [],
    financialuser: [],
    fiscalYear: [],
    registerUser: null,
    priorities: [],
    processList: [],
    projectList: [],
    projectMinistry: [],
    totalPlanCostArray:[],
    totalActulCostArray: [],
    totalVariationArray: [],
    totalPartciapArray: [],
    totalPlanCostArrayProj:[],
    totalActulCostArrayProj: [],
    totalVariationArrayProj: [],
    totalPartciapArrayPerc: [],
    totalProjectArrayPerc: [],
    dataHeading:[],
    dataHeirarchy: [],
    processDataView:[],
    projectDataView:[],
    totalPlan: 0,
    totalActul:0,
    totalVariance:0,
    totalUnutilize:0,
    variancePct:0,
    perfPct:0,
    progPct:0,
    fiscalDates : '',
    title: '',
    message: '',
    notifications: [],
    notificationsInbox: [],
  },
  mutations: {
    SET_MTP(state, mtp) {
      state.kpi_mtp = mtp.map(function (item) {
        return {
          value: item.id,
          text: item.name
        }
      });
    },
    SET_DEFAULT_DATA(state, defaultData) {
      state.sectors = defaultData.sectors;
      state.communicationuser = defaultData.communicationuser;
      state.financialuser = defaultData.financialuser;
    },
    SET_APPROVE_ORG_USER(state, defaultData) {
      state.orgapproveuser = defaultData.orgapproveuser;
      state.fiscalYear = defaultData.fiscalYear;
      state.registerUser = defaultData.registerUser
      state.processList = defaultData.processList
      state.projectList = defaultData.projectList,
      state.projectMinistry = defaultData.projectMinistry,
      state.totalPlanCostArray = defaultData.getCostP,
      state.totalActulCostArray = defaultData.getCostA,
      state.totalVariationArray = defaultData.getCostV,
      state.totalPlanCostArrayProj = defaultData.getCostPA,
      state.totalActulCostArrayProj = defaultData.getCostAA,
      state.totalVariationArrayProj = defaultData.getCostVA,

      state.totalPartciapArray = defaultData.getPercentage,
      state.totalPartciapArrayPerc = defaultData.getPercentagePartA
      state.totalProjectArrayPerc = defaultData.getPercentageProjA

    },
    SET_STP_VIEW_DEFAULT(state, defaultData) {
      state.kpi_mtp = defaultData.mtp.map(function (item) {
        return {
          value: item.id,
          text: item.name
        }
      });
      //state.setTenant = defaultData.sectors;
      state.fiscalYear = defaultData.fiscalYear;
      state.dataHeading = defaultData.dataHeading;
      state.dataHeirarchy = defaultData.dataHeirarchy;
      state.fiscalDates = defaultData.fiscalDates;
    },
    SET_STP_PROJECT_PROCESS_DATA(state, data) {
    //alert(data.dataProcessProject.length)
      if(data.dataProcessProject.length > 0) {
        state.projectDataView = data.dataProcessProject.filter(data => {
          return data.item_type != null ? data.item_type.toString().includes('project') : ''
        });

        state.processDataView = data.dataProcessProject.filter(data => {
          return data.item_type != null ? data.item_type.toString().includes('process') : ''
        });
        state.totalPlan = data.totalPlan;
        state.totalActul = data.totalActul;
        state.totalVariance = data.totalVariance;
        state.totalUnutilize = data.totalUnutilize;
        state.variancePct = data.variancePct;
        state.perfPct = data.perfPct;
        state.progPct = data.progPct;

      } else {
        state.processDataView = [];
        state.projectDataView = [];
        state.totalPlan = data.totalPlan;
        state.totalActul = 0;
        state.totalVariance = 0;
        state.totalUnutilize = 0;
        state.variancePct = 0;
        state.perfPct = 0;
        state.progPct = 0;

      }
    },
    SET_STP_DATA(state, priorities) {
      state.priorities = priorities
    },
    GET_TEXT_TITLE(state, text) {
      return state.title = text;
    },
    GET_TEXT_MSG(state, text) {
      return state.message = text;
    },
    NEW_STP_NOTIFICATION(state, [message, val]) {
      //state.notifications.unshift(JSON.parse(message))

      Vue.axios.get(`/getAllNotification`)
        .then((response) => {
          // state.notifications = response.data.data;
          state.notificationsInbox = response.data.inboxData;
          state.notifications = response.data.alertData;
        });

      // state.messageRec.unshift(message)
      console.log('stp update console');
      /*Vue.axios.get(`/loadKpiDataSector/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              state.priorities = (response.data.kpidata);
          }
        });*/

      Vue.axios.get(`/loadStpDataSector/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            state.priorities = response.data.kpidata
            //commit('SET_STP_DATA', (response.data.kpidata))
          }
          //this.state.isLoading = false;
        });
    },
  },
  computed: mapState({
    sectors: state => state.sectors,
    communicationuser: state => state.communicationuser,
    financialuser: state => state.financialuser,
    orgapproveuser: state => state.orgapproveuser,
    fiscalYear: state => state.fiscalYear,
    registerUser: state => state.registerUser,
    priorities: state => state.priorities,
    processList: state => state.processList,
    projectList: state => state.projectList,
    projectMinistry: state => state.projectMinistry,
    totalPlanCostArray: state => state.totalPlanCostArray,
    totalActulCostArray: state => state.totalActulCostArray,
    totalVariationArray: state => state.totalVariationArray,
    totalPlanCostArrayProj: state => state.totalPlanCostArrayProj,
    totalActulCostArrayProj: state => state.totalActulCostArrayProj,
    totalVariationArrayProj: state => state.totalVariationArrayProj,
    dataHeading: state => state.dataHeading,
    dataHeirarchy: state => state.dataHeirarchy,
    totalPartciapArray: state => state.totalPartciapArray,
    totalPartciapArrayPerc: state => state.totalPartciapArrayPerc,
    totalProjectArrayPerc: state => state.totalProjectArrayPerc,
    processDataView: state => state.processDataView,
    projectDataView: state => state.projectDataView,
    totalPlan: state => state.totalPlan,
    totalActul: state => state.totalActul,
    totalVariance: state => state.totalVariance,
    totalUnutilize: state => state.totalUnutilize,
    variancePct: state => state.variancePct,
    perfPct: state => state.perfPct,
    progPct: state => state.progPct,
    fiscalDates: state => state.fiscalDates
  }),
  getters: {
    sectors: state => state.sectors,
    communicationuser: state => state.communicationuser,
    financialuser: state => state.financialuser,
    orgapproveuser: state => state.orgapproveuser,
    fiscalYear: state => state.fiscalYear,
    registerUser: state => state.registerUser,
    priorities: state => state.priorities,
    processList: state => state.processList,
    projectList: state => state.projectList,
    totalPlanCostArray: state => state.totalPlanCostArray,
    totalActulCostArray: state => state.totalActulCostArray,
    totalVariationArray: state => state.totalVariationArray,
    totalPlanCostArrayProj: state => state.totalPlanCostArrayProj,
    totalActulCostArrayProj: state => state.totalActulCostArrayProj,
    totalVariationArrayProj: state => state.totalVariationArrayProj,
    dataHeading: state => state.dataHeading,
    dataHeirarchy: state => state.dataHeirarchy,
    totalPartciapArray: state => state.totalPartciapArray,
    totalPartciapArrayPerc: state => state.totalPartciapArrayPerc,
    totalProjectArrayPerc: state => state.totalProjectArrayPerc,
    processDataView: state => state.processDataView,
    projectDataView: state => state.projectDataView,
    totalPlan: state => state.totalPlan,
    totalActul: state => state.totalActul,
    totalVariance: state => state.totalVariance,
    totalUnutilize: state => state.totalUnutilize,
    variancePct: state => state.variancePct,
    perfPct: state => state.perfPct,
    progPct: state => state.progPct,
    fiscalDates: state => state.fiscalDates
  },
  actions: {
    translation() {
      Vue.axios.get(`/gettranslations`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          var trans = JSON.stringify(response.data.data);
          //console.log("testing")
          //console.log(trans);
          this.state.translations = response.data.data;
        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    get_text_title({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_TITLE', transtext)
        //return ;
      } else {
        commit('GET_TEXT_TITLE', "!" + textbit)
      }

    },
    get_text_message({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_MSG', transtext)
        //return ;
      } else {
        commit('GET_TEXT_MSG', "!" + textbit)
      }

    },
    loadMTP({commit}) {
      Vue.axios({
        url: 'mtp',
        method: 'GET'
      })
      .then(response => {
        if (response.data.code == 200) {
          //this.state.loading = true,
          commit('SET_MTP', (response.data.data))
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadSTPDefaultData({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'loadSTPDefaultData',
        method: 'GET'
      })
      .then(response => {
        if (response.data.code == 200) {
          console.log(response.data)
          this.state.loading = true,
            commit('SET_DEFAULT_DATA', (response.data))
        }
        this.state.isLoading = false;
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadSTPUserBySector({commit}, [org, mtp, stpid, fy_id]) {
      Vue.axios({
        url: `/loadSTPUserFiscalYearBySector/${org}/${mtp}/${stpid}/${fy_id}`,
        method: 'GET'
      })
      .then(response => {
        if (response.data.code == 200) {
          console.log(response.data)
          this.state.loading = true,
          commit('SET_APPROVE_ORG_USER', (response.data))
        }
        this.state.isLoading = false;
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadDefaultSectorOrgMtpFiscal({commit}, [org, mtp, period, fiscal, fromList]) {
      this.state.isLoading = true;
      Vue.axios({
        url: `/loadDefaultSectorOrgMtpFiscal/${org}/${mtp}/${period}/${fiscal}/${fromList}`,
        method: 'GET'
      })
      .then(response => {
        if (response.data.code == 200) {
          console.log(response.data)
          this.state.loading = true,
          commit('SET_STP_VIEW_DEFAULT', (response.data))
        }
        this.state.isLoading = false;
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadProcessProjectData({commit}, [org, mtp, period, fiscal]) {
      Vue.axios({
        url: `/loadProcessProjectData/${org}/${mtp}/${period}/${fiscal}`,
        method: 'GET'
      })
      .then(response => {
        if (response.data.code == 200) {
          console.log(response.data)
          this.state.loading = true,
          commit('SET_STP_PROJECT_PROCESS_DATA', (response.data))
        }
        this.state.isLoading = false;
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadSectorOrg({commit}, sid) {
      Vue.axios.get(`/loadSectorOrg/${sid}`)
      .then(response => {
        if (response.data.code == 200) {
          //this.state.loading = true,
          this.state.setTenant = response.data.sectors;

        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadStpDataSector({commit}, val) {
      this.state.isLoading = true;
      Vue.axios.get(`/loadStpDataSector/${val}`)
      .then(response => {
        if (response.data.code == 200) {
          this.state.loading = true,
          commit('SET_STP_DATA', (response.data.kpidata))
        }
        this.state.isLoading = false;
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    removestpPermanently({commit}, stpId) {
      this.state.isLoading = true;
      Vue.axios.delete(`/removestpPermanently/${stpId}`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          //console.log('removed user', response.data.code, 'from the server')
          this.dispatch('get_text_title', 'success@m');
          this.dispatch('get_text_message', response.data.msg+'@m');
          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          });
          this.dispatch('loadPriorities');
        } else {
          this.dispatch('get_text_title', 'error@m');
          this.dispatch('get_text_message', response.data.msg+'@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    updateEmpCount({commit}, [orgId, value]) {
      Vue.axios({
        url: 'updateEmpCount',
        method: 'POST',
        data: {orgId: orgId, value:value}
      }).then(response => {
         this.state.registerUser = value;
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    getNotificationStpData: ({commit}, [message, id]) => {
      commit('NEW_STP_NOTIFICATION', [message, id]);
    },
  }
})
