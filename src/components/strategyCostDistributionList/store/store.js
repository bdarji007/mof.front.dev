import Vue from 'vue'
import Vuex from 'vuex'
import Element from 'element-ui'
import {Notification} from 'element-ui'
import {i18n} from "../../../plugins/i18n";

Vue.use(Vuex);
Vue.use(i18n);
Vue.use(Element);
Vue.prototype.$notify = Notification;

export const store = new Vuex.Store({
  state: {
    translations: {
      en: {
        "hello": "Hello World"
      },
      ar: {
        "hello": "Hallo Verden"
      }
    },
    loading: false,
    linkedlist: [],
    linkingPrcId: [],
    strategylist: [],
    userProfile: [],
    groups: [],
    isLoading: false,
    fullPage: true,
    title: '',
    message: '',
    setSubTenant: [],
    setSubTenant1: [],
    setTenant: [],
    setTenant1: [],
    setTenantreal1: [],
    setObjcat: [],
    setObjective: [],


    setStrategyCategory: [],
    isTableSet: false,
    actualClick: false,
    initialClick: true,

  },
  mutations: {
    SET_STRATEGIES(state, strategylist) {
      state.strategylist = strategylist
      this.state.isTableSet = true;

      // alert(state.processlist[0]['id'])


    },

    SET_LINKS(state, linkedlist) {
      state.linkedlist = linkedlist
    },


    REMOVE_LINK(state, strategy) {
      var strategies = this.state.linkedlist
      strategies.splice(strategies.indexOf(strategy), 1)


    },

    REMOVE_STRATEGY(state) {
      var links = this.state.strategylist
      //state.linkingPrcId=this.state.linkingPrcId
      this.dispatch('loadStrategyListTable', [this.state.linkingPrcId])
      this.dispatch('loadLinkedListTable')


    },

    UPDATE_LINK(state, strategylist) {
      // state.strategylist = strategylist
      // alert('hi')
      this.dispatch('loadLinkedListTable')
    },

    CREATE_LINK(state, prc_id) {
      state.linkingPrcId = prc_id

      this.dispatch('loadLinkedListTable')
      this.dispatch('loadStrategyListTable', [this.state.linkingPrcId])

    },

    CREATED_STRATEGY(state, prc_id) {
      // console.log('add User', userObject)
      state.linkingPrcId = prc_id

      this.dispatch('loadStrategyListTable', [this.state.linkingPrcId])
      // console.log('after User', state.STRATEGIES)
    },
    GET_TEXT_TITLE(state, text) {
      return state.title = text;
    },
    GET_TEXT_MSG(state, text) {
      return state.message = text;
    },
  },
  getters: {

    strategylist: state => state.strategylist,
    getRowCount: state => state.strategylist.length,
    linkingPrcId: state => state.linkingPrcId,
    isTableSet: state => state.isTableSet,
    linkedlist: state => state.linkedlist,
    getRowCount1: state => state.linkedlist.length,
    translations: state => state.translations,
    initialClick: state => state.initialClick,
    actualClick: state => state.actualClick,
    setObjective: state => state.setObjective,

  },
  actions: {
    get_text_title({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_TITLE', transtext)
        //return ;
      } else {
        commit('GET_TEXT_TITLE', "!" + textbit)
      }

    },
    get_text_message({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_MSG', transtext)
        //return ;
      } else {
        commit('GET_TEXT_MSG', "!" + textbit)
      }

    },
    loadTenants({commit}) {
      Vue.axios({
        url: 'loadTenants',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.setTenant = response.data.tenants.map(function (item) {
              return {
                text: item.name,
                value: item.id
              }
            });

          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadObjectives({commit}) {
      Vue.axios({
        url: 'stgloadObjectives_cd',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.setObjective = response.data.tenants.map(function (item) {
              return {
                text: item.name,
                value: item.id
              }
            });

          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },


    loadlinkTenants({commit}) {
      Vue.axios({
        url: 'stgloadTenants11_cd',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.setTenant1 = response.data.tenants.map(function (item) {
              return {
                text: item.name,
                value: item.id
              }
            });

          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadObjcat({commit}) {
      Vue.axios({
        url: 'stgloadObjcat11_cd',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.setObjcat = response.data.tenants;
            // .map(function (item) {
            // return {
            //   text: item.name,
            //   value: item.id
            // }
            // });

          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadlinkTenantsreal({commit}) {
      Vue.axios({
        url: 'stgloadTenantsreal11_cd',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.setTenantreal1 = response.data.tenants.map(function (item) {
              return {
                text: item.name,
                value: item.id
              }
            });

          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
      // console.log(this.state.setTenantreal1[0].text)
    },

    loadCategory({commit}) {
      Vue.axios({
        url: 'stgloadCategory1_cd',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            this.state.setStrategyCategory = response.data.kpiCat
//             .map(function (item) {
//               // console.log(this.$i18n.locale)
// // if(item.name=="Vision"){item.name="a"}
//               return {
//                 text: item.name,//$i18n.locale=='ar'?(item.name=="Goal"?"هدف":(item.name=="Target"?"غاية":(item.name=="Vision"?"رؤية":""))):item.name, //item.name,
//                 value: item.id
//               }
//             });
            // console.log(this.state.setStrategyCategory[0].text)
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadSubTenants({commit}, val) {
      Vue.axios.get(`/stgloadSubTenants1_cd/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            this.state.setSubTenant = response.data.tenants.map(function (item) {
              //alert(item.id)
              return {
                text: item.name,
                value: item.id
              }
            });
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadlinkSubTenants({commit}, val) {
      Vue.axios.get(`/stgloadSubTenants11_cd/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            this.state.setSubTenant1 = response.data.tenants.map(function (item) {
              //alert(item.id)
              return {
                text: item.name,
                value: item.id
              }
            });
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },


    loadStrategyListTable({commit}, [prc_id]) {
      //this.state.isLoading = true;
      this.state.isTableSet = false;
      if (prc_id == undefined) {
        prc_id = this.state.linkingPrcId
      }//initial,when sector not selected
// alert(prc_id);
      // this.state.linkedlist=[];


      // if (parent == undefined) {
      //   parent=2       }//initial,when sector not selected

      Vue.axios.get(`/strategylistTableVal_cd/${prc_id}`)

        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_STRATEGIES', (response.data.data))
          } else {
            Vue.prototype.$notify.error({
              title: 'خطأ',
              message: response.data.msg,
              type: 'Error',
              position: 'top-right',
              duration: 10000

            });
          }
          this.state.isLoading = false

        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadStrategyListTableSector({commit}, [parent]) {
      this.state.isLoading = true;
      this.state.isTableSet = false

      // if (parent == undefined) {
      //   parent=2       }//initial,when sector not selected

      Vue.axios.get(`/strategylistTableValSector_cd/${parent}`)

        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_LINKS', (response.data.data))
          } else {
            Vue.prototype.$notify.error({
              title: 'خطأ',
              message: response.data.msg,
              type: 'Error',
              position: 'top-right',
              duration: 10000

            });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadStrategyListTableOrg({commit}, [parent]) {
      this.state.isLoading = true;
      // if (parent == undefined) {
      //   parent=2       }//initial,when sector not selected

      Vue.axios.get(`/strategylistTableValOrg_cd/${parent}`)

        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_LINKS', (response.data.data))
          } else {
            Vue.prototype.$notify.error({
              title: 'خطأ',
              message: response.data.msg,
              type: 'Error',
              position: 'top-right',
              duration: 10000

            });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    removeLink({commit}, link) {
      this.state.isLoading = true;
      // alert(strategy.id);
      Vue.axios.delete(`/strategylinkedlist1_cd/${link.id}`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {


          Vue.prototype.$notify({
            title: 'نجاح',
            message: response.data.msg,
            type: 'success',
            position: 'top-right',
            duration: 10000

          });
          commit('REMOVE_LINK', link)
        } else {
          // this.dispatch('get_text_title', 'Error@m');
          // this.dispatch('get_text_message', response.data.msg+'@m');
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000

          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    updateLink({commit}, link) {
      // console.log(user);
      //return false;
      this.state.isLoading = true;
      Vue.axios.put(`strategylist11_cd/${link.where}`, link.data).then(response => {
        this.state.isLoading = false;
        // this.dispatch('get_text_title', 'success@m');
        // this.dispatch('get_text_message', response.data.msg+'@m');
        // console.log('Update user', response.data.code, 'from the server')
        Vue.prototype.$notify({
          title: 'نجاح',
          message: response.data.msg,
          type: 'success',
          position: 'top-right',
          duration: 10000
        });
        commit('UPDATE_LINK', link.data)
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 201) {
          // his.dispatch('get_text_title', 'Error@m');
          // this.dispatch('get_text_message', error.response.data.errors.Type[0]+'@m');
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: error.response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000
          });
        }
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    createStrategy({commit}, strategy) {
      this.state.isLoading = true;
      console.log(strategy.data);
      Vue.axios.put(`/strategylist1_cd/${strategy.where}`, strategy.data).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          // this.dispatch('get_text_title', 'success@m');
          // this.dispatch('get_text_message', response.data.msg+'@m');
          // console.log('Created user', response.data.code, 'from the server')
          Vue.prototype.$notify({
            title: 'نجاح',
            message: response.data.msg,
            type: 'success',
            position: 'top-right',
            duration: 10000

          });
          commit('CREATED_STRATEGY', strategy.where)
        } else {
          // this.dispatch('get_text_title', 'Error@m');
          // this.dispatch('get_text_message', response.data.msg+'@m');
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000

          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 201) {
          // this.dispatch('get_text_title', 'Error@m');
          // this.dispatch('get_text_message', error.response.data.errors.Type[0]+'@m');
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000

          });
        }
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
//link table
    loadLinkedListTable({commit}) {//
      this.state.isLoading = true;
      Vue.axios.get(`/strategylinkedlistTableVal_cd`)///

        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              // alert(response.data.code);

              commit('SET_LINKS', (response.data.data))
          } else {
            // this.dispatch('get_text_title', 'Error@m');
            // this.dispatch('get_text_message', response.data.msg+'@m');
            Vue.prototype.$notify.error({
              title: 'خطأ',
              message: response.data.msg,
              type: 'Error',
              position: 'top-right',
              duration: 10000

            });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    removeStrategy({commit}, strategy_id) {
      this.state.isLoading = true;
      if (strategy_id == undefined) {
        strategy_id = this.state.linkingPrcId
      }
      //this.state.linkingPrcId=strategy_id;
      Vue.axios.delete(`/strategylist1_cd/${strategy_id}`).then(response => {

        console.log(strategy_id)
        this.state.isLoading = false;
        if (response.data.code == 200) {
          Vue.prototype.$notify({
            title: 'نجاح',
            message: response.data.msg,
            type: 'success',
            position: 'top-right',
            duration: 10000
          });
          commit('REMOVE_STRATEGY')

        } else {
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000

          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    createLink({commit}, link) {
      this.state.isLoading = true;
      if (link.where == undefined)
        link.where = this.state.linkingPrcId
      else
        this.state.linkingPrcId = link.where;
      Vue.axios.put(`strategylinkedlist1_cd/${link.where}`, link.data).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {

          Vue.prototype.$notify({
            title: 'نجاح',
            message: response.data.msg,
            type: 'success',
            position: 'top-right',
            duration: 10000
          });
        } else {
          // this.dispatch('get_text_title', 'Error@m');
          // this.dispatch('get_text_message', response.data.msg+'@m');
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000

          })
        }
        commit('CREATE_LINK', this.state.linkingPrcId)
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.data == 201) {
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: error.response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000
          });
        }
        if (error.response.data == 202) {
          Vue.prototype.$notify.error({
            title: "objective cost issue",
            message: error.response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000
          });
        }

        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },

  }
})
