import Vue from 'vue'
import Vuex from 'vuex'
import Element from 'element-ui'
import {Notification} from 'element-ui'
import {i18n} from "../../../plugins/i18n";

Vue.use(Vuex);
Vue.use(Element);
Vue.prototype.$notify = Notification;

export const store = new Vuex.Store({
  state: {
    translations: {
      en: {
        "hello": "Hello World"
      },
      ar: {
        "hello": "Hallo Verden"
      }
    },
    loading: false,
    roles: [],
    permissions: [],
    objects: [],
    systemapps: [],
    isLoading: false,
    fullPage: true,
    title: '',
    message: '',
    sectors: []
  },
  mutations: {
    SET_ROLES(state, roles) {
      state.roles = roles
    },
    SET_DEFAULT_DATA(state, defaultData) {
      state.sectors = defaultData.sectors;
    },
    SET_PERMISSIONS(state, premissions) {
      state.permissions = premissions;
    },
    SET_OBJECT_APP(state, objects) {
      state.objects = objects.objectModel;
      //state.systemapps = objects.getApps;
      state.systemapps = objects.getApps.map(function (item) {
        //alert(item);
        return {
          id: item.id,
          label: item.name
        }
      });
    },
    CREATED_GROUP(state, groupObject) {
      console.log('add Group', groupObject)
      this.dispatch('loadRoles')
      console.log('after Group', state.groups)
    },
    UPDATE_GROUP(state, group) {
      console.log(group);
      //this.dispatch('loadUsers')
    },
    REMOVE_GROUP(state, user) {
      //var groups = state.roles;
      //groups.splice(groups.indexOf(user), 1)
      this.dispatch('loadRoles')
    },
    GET_TEXT_TITLE(state, text) {
      return state.title = text;
    },
    GET_TEXT_MSG(state, text) {
      return state.message = text;
    },
  },
  getters: {
    roles: state => state.roles,
    permissions: state => state.permissions,
    objects: state => state.objects,
    systemapps: state => state.systemapps,
    sectors: state => state.sectors,
    getRowCount: state => state.roles.length
  },
  actions: {
    loadNotificationDefaultData({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'loadNotificationDefaultData1',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            console.log(response.data)
            this.state.loading = true,
              commit('SET_DEFAULT_DATA', (response.data))
          }
          this.state.isLoading = false;
        });
    },
    get_text_title({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_TITLE', transtext)
        //return ;
      } else {
        commit('GET_TEXT_TITLE', "!" + textbit)
      }

    },
    get_text_message({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_MSG', transtext)
        //return ;
      } else {
        commit('GET_TEXT_MSG', "!" + textbit)
      }

    },
    loadRoles({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'roles',
        method: 'GET'
      })
        .then(response => {
          this.state.isLoading = false;
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_ROLES', (response.data.roles))
          } else {
            this.dispatch('get_text_title', 'Error@m');
            this.dispatch('get_text_message', response.data.msg + '@m');

            Vue.prototype.$notify.error({
              title: this.state.title,
              message: this.state.message,
              type: 'Error',
              position: 'top-right'
            });
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    loadPermissions({commit}) {
      Vue.axios({
        url: 'getPermissions',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_PERMISSIONS', (response.data.permissions))
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadRoleObject({commit}) {
      Vue.axios({
        url: 'rolesObject',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_OBJECT_APP', (response.data))
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    createGroup({commit}, groupTenant) {
      this.state.isLoading = true;
      Vue.axios.post(`/roles`, groupTenant.data).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          console.log('Created Group', response.data.code, 'from the server')
          this.dispatch('get_text_title', 'success@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          })
          window.location.replace('/roles');
          commit('CREATED_GROUP', groupTenant.data)
        } else {
          console.log('Created Group', response.data.code, 'from the server')
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    updateGroup({commit}, groupTenant) {
      this.state.isLoading = true;
      Vue.axios.put(`/roles/${groupTenant.where}`, groupTenant.data).then(response => {
        this.state.isLoading = false;
        console.log('Update group', response.data.code, 'from the server')
        this.dispatch('get_text_title', 'success@m');
        this.dispatch('get_text_message', response.data.msg + '@m');
        Vue.prototype.$notify({
          title: this.state.title,
          message: this.state.message,
          type: 'success',
          position: 'top-right'
        })
        window.location.replace('/roles');
        commit('UPDATE_GROUP', groupTenant.data)
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    removeUser({commit}, groupTenant) {
      this.state.isLoading = true;
      Vue.axios.delete(`/roles/${groupTenant.id}`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          console.log('removed group', response.data.code, 'from the server')
          this.dispatch('get_text_title', 'success@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          })
          commit('REMOVE_GROUP', groupTenant)
        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    translation() {
      Vue.axios.get(`/gettranslations`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          var trans = JSON.stringify(response.data.data);
          //console.log("testing")
          //console.log(trans);
          this.state.translations = response.data.data;
        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    }
  }
})
