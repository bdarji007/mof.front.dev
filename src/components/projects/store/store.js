import Vue from 'vue'
import Vuex from 'vuex'
import Element from 'element-ui'
import {Notification} from 'element-ui'
import {i18n} from "../../../plugins/i18n";

Vue.use(Vuex);
Vue.use(i18n);
Vue.use(Element);
Vue.prototype.$notify = Notification;

export const store = new Vuex.Store({
  state: {
    translations: {
      en: {
        "hello": "Hello World"
      },
      ar: {
        "hello": "Hallo Verden"
      }
    },
    loading: false,
    projectlist: [],
    readingslist: [],
    remarkreadingslist: [],
    userProfile: [],
    groups: [],
    isLoading: false,
    fullPage: true,
    title: '',
    message: '',
    setTenant: [],
    setSubTenant: [],
    sectors: [],
    sectorUser: [],
    sectorspm: [],
    sectorUserpm: [],
    sectorscord: [],
    sectorUsercord: [],
    sectorsstr: [],
    sectorUserstr: [],

    roles: '',
    roleList: [],
    projectcostlist: [],
    kpi_mtp: '',
    start_date: null,
    end_date: null,


  },
  mutations: {
    SET_SELECTED_DATA(state, data) {
      state.roleList = data.role;
      state.userList = data.users;
      state.userIds = data.userIds;
    },
    REMOVE_COST(state, cost) {
      var costs = state.projectcostlist
      costs.splice(costs.indexOf(cost), 1)
    },
    SET_PROJECTIST(state, projectlist) {
      state.projectlist = projectlist
    },
    SET_READINGSLIST(state, readingslist) {
      state.readingslist = readingslist
    },
    SET_READINGSLIST_REMARK(state, readingslist) {
      state.remarkreadingslist = readingslist
    },
    SET_PROJECTCOSTLIST(state, projectcostlist) {
      state.projectcostlist = projectcostlist
    },
    SET_DEFAULT_DATA(state, defaultData) {
      console.log(defaultData.roles);
      state.roles = defaultData.roles.map(function (item) {
        //alert(item);
        return {
          id: item.id,
          label: item.name
        }
      });
      state.sectors = defaultData.sectors;
    },
    SET_DEFAULTPM_DATA(state, defaultDatapm) {


      state.sectorspm = defaultDatapm.sectorspm;
      state.sectorscord = defaultDatapm.sectorspm;

    },
    SET_DEFAULTSTR_DATA(state, defaultDatastr) {


      state.sectorsstr = defaultDatastr.sectorsstr;
    },

    GET_TEXT_TITLE(state, text) {
      return state.title = text;
    },
    GET_TEXT_MSG(state, text) {
      return state.message = text;
    },
    SET_MTP(state, mtp) {
      state.kpi_mtp = mtp.map(function (item) {
        return {
          value: item.id,
          text: item.name
        }
      });
    },
    NEW_PROJ_NOTIFICATION(state, [message, val]) {
      Vue.axios.get(`/getAllNotification`)
        .then((response) => {
          // state.notifications = response.data.data;
          state.notificationsInbox = response.data.inboxData;
          state.notifications = response.data.alertData;
        });
      console.log('project update console');
      Vue.axios({
        url: 'projectfilter',
        method: 'POST',
        data: {val}
      })
        .then(response => {
          if (response.data.code == 200) {
            state.projectlist =  (response.data.projectlist)
          } else {
            state.projectlist = [];
          }
        })
    },

  },
  getters: {
    projectlist: state => state.projectlist,
    readingslist: state => state.readingslist,
    remarkreadingslist: state => state.remarkreadingslist,
    projectcostlist: state => state.projectcostlist,
    getRowCount: state => state.projectlist.length,
    translations: state => state.translations,
    sectors: state => state.sectors,
    sectorUser: state => state.sectorUser,
    sectorspm: state => state.sectorspm,
    sectorUserpm: state => state.sectorUserpm,
    sectorscord: state => state.sectorscord,
    sectorUsercord: state => state.sectorUsercord,
    sectorsstr: state => state.sectorsstr,
    sectorUserstr: state => state.sectorUserstr,

  },
  actions: {
    loadMTP({commit}) {
      Vue.axios({
        url: 'mtp',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_MTP', (response.data.data))
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadNotificationDefaultData({commit}) {

      this.state.isLoading = true;
      Vue.axios({
        url: 'loadNotificationDefaultData',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {

            console.log(response.data)
            this.state.loading = true,
              commit('SET_DEFAULT_DATA', (response.data))
          }
          this.state.isLoading = false;
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadprojectmanagerDefaultData({commit}) {

      this.state.isLoading = true;
      Vue.axios({
        url: 'loadprojectmanagerDefaultData',
        method: 'GET'
      })
        .then(response => {

          if (response.data.code == 200) {

            console.log(response.data)

            this.state.loading = true,
              commit('SET_DEFAULTPM_DATA', (response.data))
          }
          this.state.isLoading = false;
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadSectorOrg({commit}, sid) {
      Vue.axios.get(`/loadSectorOrg/${sid}`)
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            this.state.setTenant = response.data.sectors;

          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadstrategicDefaultData({commit}) {

      this.state.isLoading = true;
      Vue.axios({
        url: 'loadstrategicDefaultData',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {

            console.log(response.data)

            this.state.loading = true,
              commit('SET_DEFAULTSTR_DATA', (response.data))
          }
          this.state.isLoading = false;
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadParentTenant({commit}) {
      Vue.axios({
        url: 'allTenant',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_PARENT_TANENTS', (response.data.subTenants))
          }
        });
    },
    get_text_title({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_TITLE', transtext)
        //return ;
      } else {
        commit('GET_TEXT_TITLE', "!" + textbit)
      }

    },
    get_text_message({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_MSG', transtext)
        //return ;
      } else {
        commit('GET_TEXT_MSG', "!" + textbit)
      }

    },
    loadDataFromRole({commit}, [roles]) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'loadUserDataFromRole',
        method: 'POST',
        data: {roles: roles}
      })
        .then(response => {
          if (response.data.code == 200) {
            console.log(response.data)
            this.state.loading = true,
              commit('SET_SELECTED_DATA', (response.data))
          }
          this.state.isLoading = false;
        });
    },
    loadprojectearch({commit}, val) {

      this.state.isLoading = true;
      // Vue.axios.get(`/projectfilter/${val}`)
      Vue.axios({
        url: 'projectfilter',
        method: 'POST',
        data: {val}
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              //alert(response.data.audittrial[0].new_valueslist)
              commit('SET_PROJECTIST', (response.data.projectlist))
          } else {
            commit('SET_PROJECTIST', [])
            //   this.dispatch('get_text_title', 'Error@m');
            //   this.dispatch('get_text_message', response.data.msg+'@m');
            //   Vue.prototype.$notify.error({
            //     title: this.state.title,
            //     message: this.state.message,
            //     type: 'Error',
            //     position: 'top-right'
            //   });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadprojectcostsearch({commit}, val) {

      this.state.isLoading = true;
      // Vue.axios.get(`/projectfilter/${val}`)
      Vue.axios({
        url: 'projectcostfilter',
        method: 'POST',
        data: {val}
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              //alert(response.data.audittrial[0].new_valueslist)
              commit('SET_PROJECTCOSTLIST', (response.data.projectcostlist))
          } else {
            commit('SET_PROJECTCOSTLIST', [])
            //   this.dispatch('get_text_title', 'Error@m');
            //   this.dispatch('get_text_message', response.data.msg+'@m');
            //   Vue.prototype.$notify.error({
            //     title: this.state.title,
            //     message: this.state.message,
            //     type: 'Error',
            //     position: 'top-right'
            //   });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    removeCost({commit}, cost) {
      this.state.isLoading = true;
      // alert(strategy.id);
      Vue.axios.delete(`/projectcost/${cost}`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {


          Vue.prototype.$notify({
            title: 'نجاح',
            message: response.data.msg,
            type: 'success',
            position: 'top-right',
            duration: 10000

          });
          commit('REMOVE_COST', cost)
        } else {
          // this.dispatch('get_text_title', 'Error@m');
          // this.dispatch('get_text_message', response.data.msg+'@m');
          Vue.prototype.$notify.error({
            title: 'خطأ',
            message: response.data.msg,
            type: 'Error',
            position: 'top-right',
            duration: 10000

          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    loadprojectlist({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'projectlist',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_PROJECTIST', (response.data.projectlist))
          }
          // else {
          //     this.dispatch('get_text_title', 'Error@m');
          //     this.dispatch('get_text_message', response.data.msg+'@m');
          //     Vue.prototype.$notify.error({
          //         title: this.state.title,
          //         message: this.state.message,
          //         type: 'Error',
          //         position: 'top-right'
          //     });
          // }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadprojectcostlist({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'projectcostlist',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_PROJECTCOSTLIST', (response.data.projectcostlist))
          }
          // else {
          //     this.dispatch('get_text_title', 'Error@m');
          //     this.dispatch('get_text_message', response.data.msg+'@m');
          //     Vue.prototype.$notify.error({
          //         title: this.state.title,
          //         message: this.state.message,
          //         type: 'Error',
          //         position: 'top-right'
          //     });
          // }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadreadingslist({commit}, val) {
      this.state.isLoading = true;
      Vue.axios.get(`/readingslist/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_READINGSLIST', (response.data.readingslist))
          }
          // else {
          //     this.dispatch('get_text_title', 'Error@m');
          //     this.dispatch('get_text_message', response.data.msg+'@m');
          //     Vue.prototype.$notify.error({
          //         title: this.state.title,
          //         message: this.state.message,
          //         type: 'Error',
          //         position: 'top-right'
          //     });
          // }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadremarkreadingslist({commit}, val) {
      this.state.isLoading = true;
      Vue.axios.get(`/remarkreadingslist/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_READINGSLIST_REMARK', (response.data.readingslist))
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    createProject({commit}, project) {
      this.state.isLoading = true;
      console.log(project.data);
      Vue.axios.post(`/projectsstore`, project.data).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', response.data.msg + '@m');

          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          });
          // alert("project created successfully")
          setTimeout(() => {
            window.location.replace('/projectlist');
          }, 1000);


        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    createProjectcost({commit}, project) {
      this.state.isLoading = true;
      console.log(project.data);
      Vue.axios.post(`/projectcoststore`, project.data).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          console.log('Created project', response.data.code, 'from the server')
          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          });
          //window.location.replace('/projectcost')

        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },

    projectDeletePermanently({commit}, projectId) {
      this.state.isLoading = true;
      Vue.axios.delete(`/removeprojectPermanently/${projectId}`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          //console.log('removed user', response.data.code, 'from the server')
          this.dispatch('get_text_title', 'success@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          });
          this.dispatch('loadprojectlist');
        } else {
          this.dispatch('get_text_title', 'error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },

    createprojectvalues({commit}, projectval) {
      this.state.isLoading = true;
      console.log(projectval.data);
      Vue.axios.post(`/projectsvaluesstore`, projectval.data).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          console.log('Created project', response.data.code, 'from the server')
          Vue.prototype.$notify({
            title: this.state.title,
            message: this.state.message,
            type: 'success',
            position: 'top-right'
          });
          window.location.replace('/projectlist')

        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    loadprojectbyid({commit}, val) {

      Vue.axios.get(`/projectlistbyId/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            this.state.start_date = response.data.projectlist[0]['start_dt_base'];
            this.state.end_date = response.data.projectlist[0]['end_dt_base'];

          }

        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    // loadprojectteambyid({commit}, val) {
    //
    //    Vue.axios.get(`/projectteambyid/${val}`)
    //      .then(response => {
    //       if (response.data.code == 200) {
    //         //alert(response.data.projectlist[0]['start_dt_base'])
    //         // this.state.start_date=response.data.projectlist[0]['start_dt_base'];
    //         // this.state.end_date=response.data.projectlist[0]['end_dt_base'];
    //
    //       }
    //
    //     });
    // },

    translation() {
      Vue.axios.get(`/gettranslations`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          var trans = JSON.stringify(response.data.data);
          //console.log("testing")
          //console.log(trans);
          this.state.translations = response.data.data;
        } else {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'Error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    },
    getNotificationProjectData: ({commit}, [message, val]) => {
      commit('NEW_PROJ_NOTIFICATION', [message, val]);
    },
  }
})
