import Vue from 'vue'
import Vuex from 'vuex'
import Element from 'element-ui'
import {Notification} from 'element-ui'
import {i18n} from "../../plugins/i18n";

Vue.use(Vuex);
Vue.use(Element);
Vue.prototype.$notify = Notification;

export const store = new Vuex.Store({
  state: {
    translations: {
      en: {
        "hello": "Hello World"
      },
      ar: {
        "hello": "Hallo Verden"
      }
    },
    notificationlist: [],
    notifiStatus: [],
    notifiChannel: [],
    notifiEvent: [],
    roles: '',
    dom: '',
    notiArgs: '',
    notiEvent: '',
    userIds: [],
    roleList: [],
    userList: [],
    loading: false,
    users: [],
    userProfile: [],
    setTenant: [],
    setSubTenant: [],
    groups: [],
    parents: [],
    isLoading: false,
    fullPage: true,
    title: '',
    message: '',
    sectors: [],
    sectorUser: [],
    showInformation: []
  },
  mutations: {
    SET_NOTIFICATIONIST(state, data) {
      state.notificationlist = data.notificationlist
      state.notifiChannel = data.notifiChannel
      state.notifiEvent = data.notifiEvent
      state.notifiStatus = data.notifiStatus
    },

    SET_USER_PROFILE(state, user) {
      state.userProfile = user
    },
    SET_GROUP(state, groups) {
      state.groups = groups.map(function (item) {
        return {
          text: item.name,
          value: item.id
        }
      });
    },
    REMOVE_USER(state, user) {
      var users = state.users
      users.splice(users.indexOf(user), 1)
    },
    UPDATE_USER(state, user) {
      console.log(user);
      //this.dispatch('loadUsers')
    },
    CREATED_USER(state, userObject) {
      console.log('add User', userObject)
      this.dispatch('loadUsers')
      console.log('after User', state.users)
    },
    SET_PARENT_TANENTS(state, parents) {
      state.parents = parents;
    },
    SET_DEFAULT_DATA(state, defaultData) {
      //console.log(defaultData.roles);
      state.roles = defaultData.roles.map(function (item) {
        //alert(item);
        return {
          id: item.id,
          label: item.name
        }
      });
      state.sectors = defaultData.sectors;
      state.notiArgs = defaultData.notiArgs;
      state.dom = defaultData.dom;
      state.notiEvent = defaultData.notiEvent.map(function (item) {
        return {
          text: item.name,
          value: item.id,
        }
      });
    },
    SET_SELECTED_DATA(state, data) {
      state.roleList = data.role;
      state.userList = data.users;
      state.userIds = data.userIds;
    },
    GET_TEXT_TITLE(state, text) {
      return state.title = text;
    },
    GET_TEXT_MSG(state, text) {
      return state.message = text;
    },
  },
  getters: {
    notificationlist: state => state.notificationlist,
    notifiChannel: state => state.notifiChannel,
    notifiEvent: state => state.notifiEvent,
    notifiStatus: state => state.notifiStatus,
    userIds: state => state.userIds,
    sectors: state => state.sectors,
    sectorUser: state => state.sectorUser,
    getRowCount: state => state.users.length
  },
  actions: {
    get_text_title({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_TITLE', transtext)
        //return ;
      } else {
        commit('GET_TEXT_TITLE', "!" + textbit)
      }

    },
    get_text_message({commit}, textbit) {
      var transtext = this.state.translations[i18n.locale][textbit];
      if (transtext) {
        commit('GET_TEXT_MSG', transtext)
        //return ;
      } else {
        commit('GET_TEXT_MSG', "!" + textbit)
      }

    },
    loadTenants({commit}) {
      Vue.axios({
        url: 'loadTenants',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            this.state.setTenant = response.data.tenants.map(function (item) {
              //alert(item.id)
              return {
                text: item.name,
                value: item.id
              }
            });
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadSubTenants({commit}, val) {
      Vue.axios.get(`/loadSubTenants/${val}`)
        .then(response => {
          if (response.data.code == 200) {
            //this.state.loading = true,
            this.state.setSubTenant = response.data.tenants.map(function (item) {
              //alert(item.id)
              return {
                text: item.name,
                value: item.id
              }
            });
            ;


          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadKpiSectorUsers({commit}, [sector, orgUnit]) {
      this.state.isLoading = true
      Vue.axios.get(`loadKpiSectorUsers/${sector}/${orgUnit}`)
        .then(response => {
          if (response.data.code == 200) {
            commit('SET_USERS', (response.data.data))
          } else {
            this.dispatch('get_text_title', 'error');
            this.dispatch('get_text_message', response.data.msg);
            Vue.prototype.$notify.error({
              title: this.state.title,
              message: this.state.message,
              type: 'Error',
              position: 'top-right'
            });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadKpiOrgUsers({commit}, [orgUnit, sector]) {
      this.state.isLoading = true
      Vue.axios.get(`loadKpiOrgUsers/${orgUnit}/${sector}`)
        .then(response => {
          if (response.data.code == 200) {
            commit('SET_USERS', (response.data.data))
          } else {
            this.dispatch('get_text_title', 'error');
            this.dispatch('get_text_message', response.data.msg);
            Vue.prototype.$notify.error({
              title: this.state.title,
              message: this.state.message,
              type: 'Error',
              position: 'top-right'
            });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadParentTenant({commit}) {
      Vue.axios({
        url: 'allTenant',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_PARENT_TANENTS', (response.data.subTenants))
          }
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadNotificationDefaultData({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'loadNotificationDefaultData',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            console.log(response.data)
            this.state.loading = true,
              commit('SET_DEFAULT_DATA', (response.data))
          }
          this.state.isLoading = false;
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    loadDataFromRole({commit}, [roles]) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'loadUserDataFromRole',
        method: 'POST',
        data: {roles: roles}
      })
        .then(response => {
          if (response.data.code == 200) {
            console.log(response.data)
            this.state.loading = true,
              commit('SET_SELECTED_DATA', (response.data))
          }
          this.state.isLoading = false;
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    removeDataFromRole({commit}, index) {
      //alert(index)
      this.state.roleList.splice(index, 1);
      console.log(this.state.roleList);
    },
    loadnotificationList({commit}) {
      this.state.isLoading = true;
      Vue.axios({
        url: 'notificationlist',
        method: 'GET'
      })
        .then(response => {
          if (response.data.code == 200) {
            this.state.loading = true,
              commit('SET_NOTIFICATIONIST', (response.data))
          } else {
            this.dispatch('get_text_title', 'Error@m');
            this.dispatch('get_text_message', response.data.msg + '@m');
            Vue.prototype.$notify.error({
              title: this.state.title,
              message: this.state.message,
              type: 'Error',
              position: 'top-right'
            });
          }
          this.state.isLoading = false
        }).catch(error => {
        console.log(error.response.data.message);
        this.state.isLoading = false;
        if (error.response.status == 403) {
          this.dispatch('get_text_title', 'Success@m');
          this.dispatch('get_text_message', error.response.data.message + '@m');

          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      });
    },
    translation() {
      Vue.axios.get(`/gettranslations`).then(response => {
        this.state.isLoading = false;
        if (response.data.code == 200) {
          var trans = JSON.stringify(response.data.data);
          //console.log("testing")
          //console.log(trans);
          this.state.translations = response.data.data;
        } else {
          this.dispatch('get_text_title', 'error@m');
          this.dispatch('get_text_message', response.data.msg + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          })
        }
      }).catch(error => {
        this.state.isLoading = false;
        if (error.response.status == 422) {
          this.dispatch('get_text_title', 'error@m');
          this.dispatch('get_text_message', error.response.data.errors.Type[0] + '@m');
          Vue.prototype.$notify.error({
            title: this.state.title,
            message: this.state.message,
            type: 'Error',
            position: 'top-right'
          });
        }
      })
    }
  }
})
