import Vue from 'vue'
import Vuex from 'vuex'

import messageStore from './components/store'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    messageStore
  },
  //strict: debug
})
