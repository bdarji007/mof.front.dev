export default {
  items: [
    {
      name: 'applications_board',
      url: '/LandingPage',
      icon: 'icon-speedometer',
      badge: {
        variant: 'primary',
        text: 'NEW'
      }
    },
    {
      name: 'ministry_board',
      url: '/ministrydepartmentperformance',
      icon: 'fab fa-algolia',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'ministrydashboard-view',
    },
    {
      name: 'mtp',
      icon: 'icon-user',
      url: '#',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'mtp-view',
      children: [
        {
          name: 'mtp',
          url: '/mtp',
          icon: 'icon-user',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'mtp-view',
        },
        {
          name: 'fiscal_year',
          url: '/fiscal_year',
          icon: 'fa-clock-o',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'mtp-view',
        },
      ]
    },
    {
      name: 'kpi list',
      url: '/kpilist',
      icon: 'fa fa-crosshairs',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'kpi-view',
    },
    {
      name: 'kpi reports',
      url: '#',
      icon: 'fa fa-file-contract',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'reportskpi-view',
      children: [
        {
          name: 'kpi_status_report',
          url: '/KpiStatusReport',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-status-view',
        },
        {
          name: 'quickview',
          url: '/gaugechartreport',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-quick-view',
        },
        {
          name: 'statistics_board',
          url: '/dashboard',
          icon: 'fab fa-algolia',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-staticdashboard-view',
        },
        {
          name: 'processmap',
          url: '/mindmap',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-processmap-view',
        },
        {
          name: 'kpi_pivot_report',
          url: '/KpiPivotReport',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-latereading-view',
        },
        {
          name: 'kpi_exception_report',
          url: '/KpiExceptionReport',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-exception-view',
        },
        {
          name: 'kpi_performance_report',
          url: '/KpiPerformanceReport',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-performance-view',
        },
        {
          name: 'unit_performance_report',
          url: '/UnitPerformanceReport',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-unitperformance-view',
        },
      ]
    },
    {
      name: 'unit_performance',
      url: '#',
      icon: 'fa fa-file-contract',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'reportskpi-sectionunit-view',
      children: [
        {
          name: 'org_unit_performance',
          url: '/departmentperformance',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-sectionunitperformance-view',
        },

        {
          name: 'section_performance',
          url: '/sectionperformance',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportskpi-sectionperformance-view',
        },
      ]
    },
    /*{
      name: 'project_list',
      url: '/projectlist',
      icon: 'icon-speedometer',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'projects-list',
    },*/
    /*{
      name: 'project_dashboard',
      url: '/projectdashboard',
      icon: 'icon-speedometer',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'ministryprojects-view',
    },*/
    {
      name: 'stp_list',
      url: '/stplist',
      icon: 'icon-speedometer',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'stps-view',
      /*children: [
        {
          name: 'stp_list',
          url: '/stplist',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'stp-list',
        },
      ]*/
    },
    {
      name: 'stp_reports',
      url: '#',
      icon: 'fa fa-file-contract',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'reportsstp-view',
      children: [
        {
          name: 'startegicDirectionCost',
          url: '/startegicDirectionCost',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportsstp-strategiccost',
        },
        {
          name: 'stpView',
          url: '/stpView',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportsstp-AnnualPlanPerformanceCost-view',
        },
        {
          name: 'fiscalYearPerformance',
          url: '/fiscalYearPerformance',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportsstp-FiscalYearPerformance-view',
        },
      ]
    },
    /*{
        title: true,
        name: '',
        // class: '',
        // wrapper: {
        //     element: '',
        //     attributes: {}
        // }
    },*/
    {
      name: 'prc_type',
      url: '/priority',
      icon: 'icon-puzzle',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'prctype-view',
    },
    {
      name: 'prc_range',
      url: '/priority/range',
      icon: 'icon-user',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'prcrange-view',
    },
    /* {
         name: 'PRC',
         url: '/priority',
         icon: 'icon-layers',
         roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
         //allPermissions: 'prctype-view',
         children: [
             {
                 name: 'PRC Types',
                 url: '/priority',
                 icon: 'icon-puzzle',
             },
             {
                 name: 'PRC Range',
                 url: '/priority/range',
                 icon: 'icon-directions'
             }
         ]
     },*/

    {
      name: 'users',
      icon: 'icon-user',
      url: '#',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'user-view',
      children: [
        {
          name: 'users',
          url: '/users',
          icon: 'icon-user',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'user-view',
        },
        {
          name: 'user_login_logs',
          url: '/loginlogs',
          icon: 'fa-clock-o',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'user-view',
        },
      ]
    },
    /* {
         name: 'Update Credentials',
         url: '/updateCredentials',
         icon: 'icon-user'
     },*/
    {
      name: 'subtenant',
      url: '/tenant',
      icon: 'icon-user',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'subtenant-view',
    },
    {
      name: 'models',
      url: '/models',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'models-view',
    },
    {
      name: 'role',
      url: '/roles',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'role-view',
    },
    {
      name: 'groups',
      url: '/groups',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'group-view',
    },
    {
      name: 'task_list',
      url: '/tasklist',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'task-view',
    },

    {
      name: 'strategy_list',
      url: '/strategyList',
      icon: 'icon-puzzle',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'strategy-view',
    },

    {

      name: 'strategy_cost_distribution_list',
      url: '/strategyCostDistributionList',
      icon: 'icon-puzzle',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'strategy-costdistribution-view',
    },


    {
      name: 'process_list',
      url: '/process1',
      icon: 'icon-puzzle',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'process-view',
    },
    {
      name: 'process_category',
      url: '/processcategory',
      icon: 'icon-puzzle',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'process-category-view',
    },
    /*{
      name: 'camunda_process_list',
      url: '/process',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'process-view',
    },*/
    {
      name: 'maintenance',
      url: '/maintenance',
      icon: 'icon-speedometer',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'maintenance-view',
    },


    {
      name: 'organization_chart',
      url: '/organizationchart',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'organizationChart-view',
    },
    /*{
        name: 'gaugechart',
        url: '/gaugechart',
        icon: 'icon-list',
        roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
        allPermissions: 'gaugechart-view',
    },*/
    {
      name: 'forms',
      url: '/DynamicForms',
      icon: 'icon-list',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'forms-view',
    },
    {
      name: 'translations',
      url: '/Translations',
      icon: 'fa fa-language',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'translations-view',
    },
    {
      name: 'audittrial',
      url: '/audittrial',
      icon: 'fa fa-language',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'audittrial-view',
    },

    {
      name: 'holiday',
      url: '/holiday',
      icon: 'icon-puzzle',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'holidays-view',
    },
    {
      name: 'projects',
      url: '#',
      icon: 'fa fa-file-contract',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'projects-view',
      children: [
        {
          name: 'project_list',
          url: '/projectlist',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'projects-view',
        },
        {
          name: 'project_dashboard',
          url: '/projectdashboard',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'projects-dashboard-view',
        },


        // {
        //   name: 'project_definition',
        //   url: '/projectdefinition',
        //   icon: 'icon-speedometer',
        //   roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
        //   allPermissions: 'projects-view',
        // },
        // {
        //   name: 'project_view',
        //   url: '/projectview',
        //   icon: 'icon-speedometer',
        //   roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
        //   allPermissions: 'projects-view',
        // },
        {
          name: 'risk_list',
          url: '/risklist',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'projects-riskview',
        },
        {
          name: 'user_cost',
          url: '/usercost',
          icon: 'icon-speedometer',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'projects-usercost-view',
        },
        {
          name: 'project reports',
          url: '#',
          icon: 'fa fa-file-contract',
          roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
          allPermissions: 'reportsproject-view',
          children: [

            {
              name: 'project_risk_report',
              url: '/ProjectRiskReport',
              icon: 'icon-speedometer',
              roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
              allPermissions: 'reportsproject-risk-view',
            },

            {
              name: 'project_budget_report',
              url: '/ProjectBudgetSummaryReport',
              icon: 'icon-speedometer',
              roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
              allPermissions: 'reportsproject-budgetsummery-view',
            },

            {
              name: 'project_budget_details_report',
              url: '/ProjectBudgetDetailsReport',
              icon: 'icon-speedometer',
              roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
              allPermissions: 'reportsproject-budgetdetails-view',
            },
            // {
            //   name: 'project_team_cost_report',
            //   url: '/ProjectTeamCostReport',
            //   icon: 'icon-speedometer',
            //   roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
            //   allPermissions: 'reportskpi-view',
            // },

            {
              name: 'project_by_status_report',
              url: '/ProjectByStatusReport',
              icon: 'icon-speedometer',
              roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
              allPermissions: 'reportsproject-bystatus-view',
            },
            {
              name: 'top_worth_project_report',
              url: '/TopWorthProjectReport',
              icon: 'icon-speedometer',
              roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
              allPermissions: 'reportsproject-topworth-view',
            },


          ],
        }
      ]
    },
    /*{
      name: 'notification',
      url: '/notification',
      icon: 'fa fa-inbox',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'notification-view',
    },*/
    {
      name: 'notification',
      url: '/notificationList',
      icon: 'fa fa-file-contract',
      roles: ['Admin', 'Manager', 'Employees', 'Strategy Executive', 'Department Communicator', 'Under Secretary', 'Under Secretary Assistant'],
      allPermissions: 'notification-view',
    },

    /*{
        name: 'Colors',
        url: '/theme/colors',
        icon: 'icon-drop',
        roles: ['Admin'],
    },
    {
        name: 'Typography',
        url: '/theme/typography',
        icon: 'icon-pencil',
        roles: ['Admin'],
    },*/
    /*{
        title: true,
        name: 'Components',
        class: '',
        wrapper: {
            element: '',
            attributes: {}
        }
    },*/
    /*{
        name: 'Base',
        url: '/base',
        icon: 'icon-puzzle',
        roles: ['Admin'],
        children: [
            {
                name: 'Breadcrumbs',
                url: '/base/breadcrumbs',
                icon: 'icon-puzzle'
            },
            {
                name: 'Cards',
                url: '/base/cards',
                icon: 'icon-puzzle'
            },
            {
                name: 'Carousels',
                url: '/base/carousels',
                icon: 'icon-puzzle'
            },
            {
                name: 'Collapses',
                url: '/base/collapses',
                icon: 'icon-puzzle'
            },
            {
                name: 'Forms',
                url: '/base/forms',
                icon: 'icon-puzzle'
            },
            {
                name: 'Jumbotrons',
                url: '/base/jumbotrons',
                icon: 'icon-puzzle'
            },
            {
                name: 'List Groups',
                url: '/base/list-groups',
                icon: 'icon-puzzle'
            },
            {
                name: 'Navs',
                url: '/base/navs',
                icon: 'icon-puzzle'
            },
            {
                name: 'Navbars',
                url: '/base/navbars',
                icon: 'icon-puzzle'
            },
            {
                name: 'Paginations',
                url: '/base/paginations',
                icon: 'icon-puzzle'
            },
            {
                name: 'Popovers',
                url: '/base/popovers',
                icon: 'icon-puzzle'
            },
            {
                name: 'Progress Bars',
                url: '/base/progress-bars',
                icon: 'icon-puzzle'
            },
            {
                name: 'Switches',
                url: '/base/switches',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tables',
                url: '/base/tables',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tabs',
                url: '/base/tabs',
                icon: 'icon-puzzle'
            },
            {
                name: 'Tooltips',
                url: '/base/tooltips',
                icon: 'icon-puzzle'
            }
        ]
    },
    {
        name: 'Buttons',
        url: '/buttons',
        icon: 'icon-cursor',
        roles: ['Admin'],
        children: [
            {
                name: 'Buttons',
                url: '/buttons/standard-buttons',
                icon: 'icon-cursor'
            },
            {
                name: 'Button Dropdowns',
                url: '/buttons/dropdowns',
                icon: 'icon-cursor'
            },
            {
                name: 'Button Groups',
                url: '/buttons/button-groups',
                icon: 'icon-cursor'
            },
            {
                name: 'Brand Buttons',
                url: '/buttons/brand-buttons',
                icon: 'icon-cursor'
            }
        ]
    },
    {
        name: 'Charts',
        url: '/charts',
        icon: 'icon-pie-chart',
        roles: ['Admin'],
    },
    {
        name: 'Icons',
        url: '/icons',
        icon: 'icon-star',
        roles: ['Admin'],
        children: [
            {
                name: 'CoreUI Icons',
                url: '/icons/coreui-icons',
                icon: 'icon-star',
                badge: {
                    variant: 'info',
                    text: 'NEW'
                }
            },
            {
                name: 'Flags',
                url: '/icons/flags',
                icon: 'icon-star'
            },
            {
                name: 'Font Awesome',
                url: '/icons/font-awesome',
                icon: 'icon-star',
                badge: {
                    variant: 'secondary',
                    text: '4.7'
                }
            },
            {
                name: 'Simple Line Icons',
                url: '/icons/simple-line-icons',
                icon: 'icon-star'
            }
        ]
    },
    {
        name: 'Notifications',
        url: '/notifications',
        icon: 'icon-bell',
        roles: ['Admin'],
        children: [
            {
                name: 'Alerts',
                url: '/notifications/alerts',
                icon: 'icon-bell'
            },
            {
                name: 'Badges',
                url: '/notifications/badges',
                icon: 'icon-bell'
            },
            {
                name: 'Modals',
                url: '/notifications/modals',
                icon: 'icon-bell'
            }
        ]
    },
    {
        name: 'Widgets',
        url: '/widgets',
        icon: 'icon-calculator',
        roles: ['Admin'],
        badge: {
            variant: 'primary',
            text: 'NEW'
        }
    },
    {
        divider: true
    },
    /!*{
        title: true,
        name: 'Extras',
    },*!/
    {
        name: 'Pages',
        url: '/pages',
        icon: 'icon-star',
        roles: ['Admin'],
        children: [
            {
                name: 'Login',
                url: '/pages/login',
                icon: 'icon-star'
            },
            {
                name: 'Register',
                url: '/pages/register',
                icon: 'icon-star'
            },
            {
                name: 'Error 404',
                url: '/pages/404',
                icon: 'icon-star'
            },
            {
                name: 'Error 500',
                url: '/pages/500',
                icon: 'icon-star'
            }
        ]
    },
    {
        name: 'Download CoreUI',
        url: 'http://coreui.io/vue/',
        icon: 'icon-cloud-download',
        roles: ['Admin'],
        class: 'mt-auto',
        variant: 'success'
    },
    {
        name: 'Try CoreUI PRO',
        url: 'http://coreui.io/pro/vue/',
        icon: 'icon-layers',
        roles: ['Admin'],
        variant: 'danger'
    }*/
  ]
}
